const pkg = require("./package");

module.exports = {
  mode: "universal",

  /*
   ** Headers of the page
   */
  head: {
    title: pkg.name,
    meta: [{
        charset: "utf-8"
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1"
      },
      {
        hid: "description",
        name: "description",
        content: pkg.description
      }
    ],
    link: [{
      rel: "icon",
      type: "image/x-icon",
      href: "/favicon.ico"
    }]
  },

  /*
   ** Translation middleware set at router
   */
  router: {
    middleware: [
      //"cookie",
      "i18n"
    ]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: '~/components/loading/index.vue',

  /*
   ** Global CSS
   */
  css: ["~/assets/style/reset.scss", "~/assets/style/base.scss"],

  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    "~/plugins/i18n.js",
    "~/plugins/axios.js",
    "~/plugins/helpers.js",
    "~/plugins/currency.js",
	"~/plugins/vueanimate.js",
	"~/plugins/string-filter.js",
	"~/plugins/cloudinary.js",
	{ src: "~/plugins/vueslider.js", ssr: false },
	{ src: "~/plugins/carousel.js", ssr: false },
	{ src: "~/plugins/crisp.js", ssr: false }
  ],

  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    "@nuxtjs/axios",
    "@nuxtjs/dotenv",
    '@nuxtjs/moment',
    'nuxt-google-maps-module'
  ],

  module: {
    rules: [{
        enforce: "pre",
        test: /\.(js|vue)$/,
        loader: "eslint-loader",
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        use: ["vue-style-loader", "css-loader", "sass-loader"]
      }
    ]
  },

  /*
   ** Axios module configuration
   */
  axios: {

  },

  /*
   ** Google Maps API Key
   */
  maps: {
    key: 'AIzaSyAZcrK47lWArWHYXc0mVGpQH_iFPsVBUcw',
  },

  /*
   ** Build configuration
   */
  build: {
    /*
     ** You can extend webpack config here
     */
    extend(config, {
      isClient
    }) {
      vendor: ["vue-i18n"];
      if (isClient) {
        config.devtool = '#source-map'
      }
    },
    analyze: false
  }
};
