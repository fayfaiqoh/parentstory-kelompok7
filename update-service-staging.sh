TASK_REVISION=`aws ecs describe-task-definition --task-definition parentstoryweb-staging-task | egrep "revision" | tr "/" " " | awk '{ print $2 }' | sed 's/,$//'`
DESIRED_COUNT=`aws ecs describe-services --services arn:aws:ecs:ap-southeast-1:922278837503:service/parentstoryweb-staging-service --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/frontend-staging-cluster-private | egrep "desiredCount" | tr "/" " " | awk '{print $2}' | sed 's/,$//' | head -n1`
if [ ${DESIRED_COUNT} = "0" ]; then
    DESIRED_COUNT="1"
fi
aws ecs update-service --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/frontend-staging-cluster-private  --service arn:aws:ecs:ap-southeast-1:922278837503:service/parentstoryweb-staging-service --task-definition parentstoryweb-staging-task:${TASK_REVISION} --desired-count ${DESIRED_COUNT}
