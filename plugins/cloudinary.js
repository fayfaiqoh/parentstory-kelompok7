import Vue from 'vue'
import VueCloudinary from 'vue-cloudinary-plugin'

Vue.use(VueCloudinary, {
	"cloud_name": process.env.CLOUDINARY_NAME,
	"api_key": process.env.CLOUDINARY_API_KEY,
	"api_secret": process.env.CLOUDINARY_SECRET,
	'secure': true,
  }, {
	"flags": "progressive",
	"dpr": "2.0",
	"crop": "fill",
	"gravity": "faces",
	"fetch_format": "auto",
	"quality": "80"
  });