const axios = require('axios')

/**
 * String functions
 */

export function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export function initialOf(string) {
	const words = string.trim().split(' ')
	let initial = ''
	for (let i = 0; i < words.length; i++) {
		if (words[i] !== '') {
			initial += words[i][0].toUpperCase()
		}
		if (initial.length == 2) break
	}
	return initial
}

export function capitalize(string) {
	if (!string) return ''
	let result = ''
	function isLowerCaseChar(ch) {
		return ch >= 'a' && ch <= 'z'
	}
	for (let i = 0; i < string.length; i++) {
		const isFirstChar = (i == 0) && isLowerCaseChar(string[i])
		const charBeforeIsSpace = (i > 0) && (string[i - 1] == ' ')
		if (isFirstChar || (charBeforeIsSpace)) {
			result += string[i].toUpperCase()
		} else {
			result += string[i]
		}
	}
	return result
}

export function removeSpace(string) {
	let result = ''
	for (let i = 0; i < string.length; i++) {
		if (string[i] == ' ') continue
		result += string[i]
	}
	return result
}

export function showCategories(categories){
	var string = ""
	categories.forEach((category,index) => {
		string+= category.name
		if (index != categories.length-1)
			string+= ", "
	})
	return string
}

export function showAgeRange(age_ranges){
	if (age_ranges.length == 1)
		return age_ranges[0].value
	else {
		//slice to void infinite loop warning on vue
		age_ranges.slice().sort(function(a, b){
			return a.id > b.id;
		}).slice(1)
		return age_ranges[0].value + " - " + age_ranges[age_ranges.length-1].value
	}
}

export function zeroPad(num, places) {
	var zero = places - num.toString().length + 1;
	return Array(+(zero > 0 && zero)).join("0") + num;
}

/**
 * Alert functions
 */

const ALERT_TIMEOUT = 2000 /* in milliseconds */

function dispatchAlert(component, category, message, autoHide, show) {
  component.$store.dispatch("showAlert", {
    category,
    message,
    autoHide,
    show
  });
}

export function showSuccessAlert(component, message, autoHide = true) {
  dispatchAlert(component, 'success', message, autoHide, true)
  if (autoHide) {
    setTimeout(() => {
      hideAlert(component)
    }, ALERT_TIMEOUT);
  }
}

export function showWarningAlert(component, message, autoHide = true) {
  dispatchAlert(component, "warning", message, autoHide, true);
  if (autoHide) {
    setTimeout(() => {
      hideAlert(component);
    }, ALERT_TIMEOUT);
  }
}

export function showErrorAlert(component, message, autoHide = true) {
  dispatchAlert(component, "error", message, autoHide, true);
  if (autoHide) {
    setTimeout(() => {
      hideAlert(component);
    }, ALERT_TIMEOUT);
  }
}

export function hideAlert(component) {
  dispatchAlert(component, "", "", false, false);
}

/**
 * Upload function
 */

export function uploadImage(file) {
  let cloudinary = {
    uploadPreset: process.env.CLOUDINARY_PRESET,
    apiKey: process.env.CLOUDINARY_API_KEY,
    cloudName: process.env.CLOUDINARY_NAME,
    baseURL: process.env.CLOUDINARY_BASE_URL
  }
  const formData = new FormData()
  formData.append('file', file[0])
  formData.append('upload_preset', cloudinary.uploadPreset)
  return axios.post(cloudinary.baseURL, formData).then((response) => {
    return response
  }, (error) => {

  })
}

/**
 * Date functions
 */

import moment from 'moment'

export function getCurrentDate() {
  return moment().format('YYYY-MM-DD')
}

export function getStartDate(monthFromCurrent = 0) {
  return moment().add(monthFromCurrent, 'months').date(1).format('YYYY-MM-DD')
}

export function getEndDate(monthFromCurrent = 0) {
  return moment().add(monthFromCurrent + 1, 'months').date(0).format('YYYY-MM-DD')
}

export function showAge(birthday) {
  	if (moment().diff(birthday, 'months') < 13)
		return moment().diff(birthday, 'months') + " Months"
	else if (moment().diff(birthday, 'months') >= 84)
		return " 6+ Years"
	return moment().diff(birthday, 'years') + " Years"
}
  


/** 
 * Pagination function
*/

export function paginations(c, m) {
	var current = c,
		last = m,
		delta = 2,
		left = current - delta,
		right = current + delta + 1,
		range = [],
		rangeWithDots = [],
		l;

	for (let i = 1; i <= last; i++) {
		if (i == 1 || i == last || i >= left && i < right) {
			range.push(i);
		}
	}

	for (let i of range) {
		if (l) {
			if (i - l === 2) {
				rangeWithDots.push(l + 1);
			} else if (i - l !== 1) {
				rangeWithDots.push('...');
			}
		}
		rangeWithDots.push(i);
		l = i;
	}

	return rangeWithDots;
}

/**
 * Image Function
 * 
 */

 export function getImageId(url){
	let string = '/'
	var i = 7;
	if (url){
		url = url.split('/')
		for (i; i<url.length;i++){
			string+=url[i]
			if (i!=url.length-1)
				string+='/'
		}
		return string
	}
 }