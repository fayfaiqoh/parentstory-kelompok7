import Vue from 'vue'

Vue.filter('capitalize', function (string) {
	if (!string) return ''
	let result = ''

	function isLowerCaseChar(ch) {
		return ch >= 'a' && ch <= 'z'
	}
	for (let i = 0; i < string.length; i++) {
		const isFirstChar = (i == 0) && isLowerCaseChar(string[i])
		const charBeforeIsSpace = (i > 0) && (string[i - 1] == ' ')
		if (isFirstChar || (charBeforeIsSpace)) {
			result += string[i].toUpperCase()
		} else {
			result += string[i]
		}
	}
	return result
})
