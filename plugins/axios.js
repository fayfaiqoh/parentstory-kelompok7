import cookieparser from 'cookieparser'
import Cookie from 'js-cookie'

export default ({store,  $axios ,redirect, router,req }) => {
	let token = {}
	$axios.baseURL = process.env.API_BASE_URL

	if (process.server && req.headers.cookie) {
		var parsed = cookieparser.parse(req.headers.cookie)
		token.accessToken = parsed.PSID
		token.refreshToken = parsed.PSIDRT
		if (parsed.PSID)
			$axios.setToken(parsed.PSID,'Bearer')
		store.commit('SET_TOKEN', token)
	}
	else if (process.client){
		token.accessToken = Cookie.get('PSID')
		token.refreshToken = Cookie.get('PSIDRT')
		if (Cookie.get('PSID'))
			$axios.setToken(Cookie.get('PSID'),'Bearer')
		store.commit('SET_TOKEN', token)
	}

	$axios.onResponseError(err => {
		if(err.response.status === 401) {
			let redirectUrl = store.state.lang.locale == 'en' ? '/logout' : '/' + store.state.lang.locale + '/logout'
			return redirect(redirectUrl)
		}
	})

}