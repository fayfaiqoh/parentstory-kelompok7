export default ({store,route }) => {

	window.$crisp = [];
	window.CRISP_WEBSITE_ID = "bba4cf2b-3802-48d2-9a6f-f604bfe3c162";
	if (store.state.account.account){
		window.$crisp.push(["set", "user:email", store.state.account.account.email]);
		window.$crisp.push(["set", "user:nickname", store.state.account.account.display_name]);
	}
	if (process.env.NODE_ENV !== 'local' && !route.path.includes('/activity/')){
		(function() {
		var d = document;
		var s = d.createElement("script");
	
		s.src = "https://client.crisp.chat/l.js";
		s.async = 1;
		d.getElementsByTagName("head")[0].appendChild(s);
		})();
	}
};