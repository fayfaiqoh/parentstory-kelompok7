const store = {
	state: () => ({
		holidays: []
	}),
	getters: {

	},
	mutations: {
		SET_HOLIDAYS(state,holidays){
			state.holidays = holidays
		}

	},
	actions: {
		setHolidays: ({commit},holidays) => {
			commit('SET_HOLIDAYS',holidays)
		}
	}
}

export default store
