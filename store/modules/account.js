import Cookie from 'js-cookie'
import cookieparser from 'cookieparser'

let DOMAIN = 'parentstory.com'

if (process.env.NODE_ENV == 'local')
	DOMAIN = 'localhost'

const store = {
	state: () => ({
		account: null,
		token: Cookie.get('PSID') || null,
		refreshToken: Cookie.get('PSIDRT') || null,
		partner: null
	}),
	getters: {

	},
	mutations: {
		LOGIN (state, account) {
			state.account = account
			state.token = account.access_token
			state.refreshToken = account.refresh_token
		},
		SET_ACCOUNT(state,account){
			state.account = account
		},
		SET_TOKEN(state,token){
			state.token = token.accessToken
			state.refreshToken = token.refreshToken
		},
		LOGOUT (state){
			state.account = null
			state.token = null
			state.refreshToken = null
		},
		SET_PARTNER(state,partner){
			state.partner = partner
		},
		VERIFY(state){
			state.account.is_verified = true
		}
	},
	actions: {
		login: ({commit}, account) => {
			Cookie.set('PSID', account.access_token,{ expires: 365*2, domain: DOMAIN })
			Cookie.set('PSIDRT', account.refresh_token,{ expires: 365*2, domain: DOMAIN })
			commit('LOGIN', account)
		},
		logout: ({commit}) => {
			commit('LOGOUT')
			Cookie.remove('PSID',{  domain: DOMAIN })
			Cookie.remove('PSIDRT',{ domain: DOMAIN })
		},
		setAccount: ({commit},account) => {
			commit('SET_ACCOUNT',account)
		},
		setToken: ({commit},token) => {
			Cookie.set('PSID', token.accessToken,{ expires: 365*2, domain: DOMAIN })
			Cookie.set('PSIDRT', token.refreshToken,{ expires: 365*2, domain: DOMAIN })
			commit('SET_TOKEN', token)
		},
		setPartner: ({commit},partner) => {
			commit('SET_PARTNER', partner)
		},
		verify: ({commit}) =>{
			commit('VERIFY')
		}
	}
}

export default store
