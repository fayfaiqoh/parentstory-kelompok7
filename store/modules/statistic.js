const store = {
	state: () => ({
		generalStatistic: {
		activity: 0,
		class: 0,
		participant: 0
		}

	}),
	getters: {},
	mutations: {
		SET_STATISTIC(state, statistic) {
			state.generalStatistic.activity = statistic.activity
			state.generalStatistic.class = statistic.class
			state.generalStatistic.participant = statistic.participant
		}

	},
	actions: {
		setStatistic: ({commit}, statistic) => {
			commit('SET_STATISTIC', statistic)
		}
	}
}

export default store
