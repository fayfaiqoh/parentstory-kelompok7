const store = {
	state: () => ({
		ageRanges: [],
		ageGroups: []
	}),
	getters: {

	},
	mutations: {
		SET_AGE_RANGES(state,ageRanges){
			state.ageRanges = ageRanges
		},
		SET_AGE_GROUPS(state,ageGroups){
			state.ageGroups = ageGroups
		}

	},
	actions: {
		setAgeRanges: ({commit},ageRanges) => {
			commit('SET_AGE_RANGES',ageRanges)
		},
		setAgeGroups: ({commit},ageGroups) => {
			commit('SET_AGE_GROUPS',ageGroups)
		}
	}
}

export default store
