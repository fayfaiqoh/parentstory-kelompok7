import moment from 'moment'

const store = {
	state: () => ({
		memberships: [],
		remainingPasses: 0,
		remainingPassesValidUntil: null,
		availableMemberships: []
	}),
	getters: {
	},
	mutations: {
		SET_MEMBERSHIPS(state, memberships) {
			state.memberships = memberships
		},
		SET_REMAINING_PASSES(state, remaining) {
			state.remainingPasses = remaining
		},
		SET_REMAINING_PASSES_VALID_UNTIL(state, untilDate) {
		  state.remainingPassesValidUntil = untilDate
		},
		SET_AVAILABLE_MEMBERSHIPS(state, availableMemberships){
			state.availableMemberships = availableMemberships
		}
	},
	actions: {
		setMemberships: ({ commit }, memberships) => {
			let remaining = 0
			let remainingPassesValidUntil = null
			for (let i = 0; i < memberships.length; i++) {
				const membership = memberships[i]
				const now = moment()
				const startDate = moment(membership.start_date)
				const endDate = moment(membership.end_date)
				const isActive = now.isSameOrAfter(startDate, 'day') &&
					now.isSameOrBefore(endDate, 'day')
				if (isActive) {
					remaining += membership.passes
					if (!remainingPassesValidUntil || remainingPassesValidUntil.isBefore(endDate)) {
						remainingPassesValidUntil = endDate
					}
				}
			}

			commit('SET_MEMBERSHIPS', memberships)
			commit('SET_REMAINING_PASSES', remaining)
			commit('SET_REMAINING_PASSES_VALID_UNTIL', remainingPassesValidUntil)
		},
		setAvailableMemberships({commit},availableMemberships){
			commit('SET_AVAILABLE_MEMBERSHIPS',availableMemberships)
		}
	}
}

export default store
