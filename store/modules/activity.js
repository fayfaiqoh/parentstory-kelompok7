const store = {
	state: () => ({
		activities: [],
		activity: null,
		classes: [],
		classesMeta: null,
		activitiesMeta: null,
		status: null,
		activityId: null,
		searchCompletion: []
	}),
	getters: {
	},
	mutations: {
		SET_ACTIVITIES(state,activities){
			state.activities = activities
		},
		SET_ACTIVITIES_META(state,activitiesMeta){
			state.activitiesMeta = activitiesMeta
		},
		SET_ACTIVITY(state,activity){
			state.activity = activity
		},
		SET_CLASSES_ACTIVITY(state,classes){
			state.classes = classes
		},
		SET_CLASSES_ACTIVITY_META(state,classesMeta){
			state.classesMeta = classesMeta
		},
		SET_ACTIVITY_STATUS(state,status){
			state.status = status
		},
		SET_ACTIVITY_ID(state,id){
			state.activityId = id
		},
		CLEAR_ACTIVITY(state){
			state.activity = null
		},
		SET_SEARCH_COMPLETION(state,searchCompletion){
			state.searchCompletion = searchCompletion
		},
		CLEAR_ACTIVITY_STATUS(state){
			state.status = null
			state.activityId = null
		}
	},
	actions: {
		setActivities: ({commit},activities) => {
			commit('SET_ACTIVITIES',activities)
		},
		setActivitiesMeta: ({commit},meta) => {
			commit('SET_ACTIVITIES_META',meta)
		},
		setActivity: ({commit},activity) => {
			commit('SET_ACTIVITY',activity)
		},
		setClassesActivity: ({commit},classes)=>{
			commit('SET_CLASSES_ACTIVITY',classes)
		},
		setClassesActivityMeta: ({commit},meta)=>{
			commit('SET_CLASSES_ACTIVITY_META',meta)
		},
		saveDraft:({commit},obj)=>{
			commit('SET_ACTIVITY_STATUS', obj.status)
			commit('SET_ACTIVITY_ID', obj.id)
		},
		saveActivity:({commit},obj)=>{
			commit('SET_ACTIVITY_STATUS', obj.status)
			commit('SET_ACTIVITY_ID', obj.id)
		},
		clearActivity:({commit})=>{
			commit('CLEAR_ACTIVITY')
		},
		clearActivityStatus:({commit})=>{
			commit("CLEAR_ACTIVITY_STATUS")
		},
		setSearchCompletion:({commit},searchCompletion)=>{
			commit('SET_SEARCH_COMPLETION',searchCompletion)
		}
	}
}

export default store
