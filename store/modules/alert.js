import Cookie from 'js-cookie'
import cookieparser from 'cookieparser'

const store = {
	state: () => ({
		category: "",
		message: "",
		autoHide: false,
		show: false
	}),
	getters: {

	},
	mutations: {
		SET_ALERT(state, alert) {
			state.category = alert.category;
			state.message = alert.message;
			state.autoHide = alert.autoHide;
			state.show = alert.show
		}
	},
	actions: {
		showAlert({ commit }, alert) {
			commit('SET_ALERT', alert)
		},
		hideAlert({ commit }) {
			commit('SET_ALERT', { show: false })
		}
	}
}

export default store
