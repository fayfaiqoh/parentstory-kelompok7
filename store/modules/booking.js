const store = {
	state: () => ({
		bookings:[],
		bookingsMeta:null,
		participants: [],
		participantsMeta: null,
		booking: null,
		transactions: []
	}),
	getters: {
	},
	mutations: {
		SET_PARTICIPANTS(state,participants){
			state.participants = participants.list
			state.participantsMeta = participants.meta
		},
		SET_BOOKINGS(state,bookings){
			state.bookings = bookings.list
			state.bookingsMeta = bookings.meta
		},
		SET_BOOKING(state,booking){
			state.booking = booking
		},
		SET_BOOKING_PRESENT(state){
			state.booking.is_present = true
		},
		SET_TRANSACTIONS(state,transactions){
			state.transactions = transactions
		},
	},
	actions: {
		setParticipants: ({commit}, participants) => {
			commit('SET_PARTICIPANTS',participants)
		},
		setBookings: ({commit}, bookings) => {
			commit('SET_BOOKINGS',bookings)
		},
		setBooking: ({commit}, booking) => {
			commit('SET_BOOKING',booking)
		},
		setBookingPresent: ({commit}) => {
			commit('SET_BOOKING_PRESENT')
		},
		setTransactions: ({commit}, transactions) => {
			commit('SET_TRANSACTIONS',transactions)
		},
	}
}

export default store
