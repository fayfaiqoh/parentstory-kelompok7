const store = {
	state: () => ({
		classes: [],
		meta: null,
		klass: null
	}),
	getters: {
	},
	mutations: {
		SET_CLASSES(state,classes){
			state.classes = classes
		},
		SET_META_CLASSES(state,meta){
			state.meta = meta
		},
		SET_CLASS(state,klass){
			state.klass = klass
		}

	},
	actions: {
		setClasses: ({commit}, classes) => {
			commit('SET_CLASSES',classes)
		},
		setMetaClasses: ({commit}, meta) => {
			commit('SET_META_CLASSES',meta)
		},
		setClass: ({commit}, klass) => {
			commit('SET_CLASS',klass)
		},
	}
}

export default store
