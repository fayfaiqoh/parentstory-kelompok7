const store = {
	state: () => ({
		categories: []
	}),
	getters: {

	},
	mutations: {
		SET_CATEGORIES(state,categories){
			state.categories = categories
		}

	},
	actions: {
		setCategories: ({commit},categories) => {
			commit('SET_CATEGORIES',categories)
		}
	}
}

export default store
