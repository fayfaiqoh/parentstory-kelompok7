const store = {
	state: () => ({
		partners: []
	}),
	getters: {

	},
	mutations: {
		SET_PARTNERS(state, partners) {
			state.partners = partners
		}
	},
	actions: {
		setPartners: ({ commit }, partners) => {
			commit('SET_PARTNERS', partners)
		}
	}
}

export default store