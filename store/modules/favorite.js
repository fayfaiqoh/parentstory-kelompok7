const store = {
	state: () => ({
		favorites: [],
		favoritesMeta: null
	}),
	getters: {

	},
	mutations: {
		SET_FAVORITES(state,favorites){
			state.favorites = favorites
		},
		SET_FAVORITES_META(state, favoritesMeta) {
			state.favoritesMeta = favoritesMeta
		}
	},
	actions: {
		setFavorites: ({commit},favorites) => {
			commit('SET_FAVORITES',favorites)
		},
		setFavoritesMeta: ({commit},favoritesMeta) => {
			commit('SET_FAVORITES_META',favoritesMeta)
		}
	}
}

export default store
