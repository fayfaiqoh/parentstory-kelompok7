const store = {
	state: () => ({
		cities: []
	}),
	getters: {

	},
	mutations: {
		SET_CITIES(state,cities){
			state.cities = cities
		}

	},
	actions: {
		setCities: ({commit},cities) => {
			commit('SET_CITIES',cities)
		}
	}
}

export default store
