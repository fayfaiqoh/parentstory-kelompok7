const store = {
	state: () => ({
		reviews: [],
		reviewsMeta: null,
	}),
	getters: {},
	mutations: {
		SET_REVIEWS(state, reviews) {
			state.reviews = reviews
		},
		SET_REVIEWS_META(state, reviewsMeta) {
			state.reviewsMeta = reviewsMeta
		}

	},
	actions: {
		setReviews: ({commit}, reviews) => {
			commit('SET_REVIEWS', reviews)
		},
		setReviewsMeta: ({commit}, reviewsMeta) => {
			commit('SET_REVIEWS_META', reviewsMeta)
		}
	}
}

export default store
