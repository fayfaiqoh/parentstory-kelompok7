import Vue from 'vue'
import Vuex from 'vuex'
import account from './modules/account'
import activity from './modules/activity'
import category from './modules/category'
import city from './modules/city'
import ageRange from './modules/ageRange'
import lang from './modules/lang'
import alert from './modules/alert'
import statistic from './modules/statistic'
import klass from './modules/klass'
import review from './modules/review'
import booking from './modules/booking'
import holiday from './modules/holiday'
import membership from './modules/membership'
import favorite from './modules/favorite'
import partners from './modules/partners'

Vue.use(Vuex)

const store = () => new Vuex.Store({
  modules: {
    account,
    activity,
    lang,
    category,
    city,
    alert,
    ageRange,
    statistic,
    klass,
    review,
	booking,
	holiday,
	membership,
	favorite,
	partners
  }
})

export default store
