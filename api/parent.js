const url = process.env.API_BASE_URL + 'ms/account'

export default class ParentAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	updateParent(data) {
		return this.$axios.request({
			'url': url + '/v1/parent',
			'method': 'PUT',
			data,
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}
}
