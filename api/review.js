const url = process.env.API_BASE_URL + 'ms/booking'

export default class ReviewAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getReviews(query="",sort=null,sort_by=null,page = 1, size = 10) {
		return this.$axios.request({
			'url': url + '/v1/me/reviews',
			'params': {
				query,
				sort,
				sort_by,
				size,
				page,
			},
			'method': 'GET',
			'headers': {
				'Content-Type': 'application/json'
			}
		})
	}
	getActivityReviews(id,page = 1, size = 10) {
		return this.$axios.request({
			'url': url + '/v1/activity/'+id+'/reviews',
			'params': {
				size,
				page
			},
			'method': 'GET',
			'headers': {
				'Content-Type': 'application/json'
			}
		})
	}
	getPartnerReviews(id,page = 1, size = 10) {
			return this.$axios.request({
			'url': url + '/v1/partner/'+id+'/reviews',
			'params': {
				size,
				page
			},
			'method': 'GET',
			'headers': {
				'Content-Type': 'application/json'
			}
		})
	}
	postReview (data) {
		return this.$axios.request({
			'url': url + '/v1/review',
			'method': 'POST',
			data,
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
