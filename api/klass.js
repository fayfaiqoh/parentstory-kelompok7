const url = process.env.API_BASE_URL + 'ms/activity'

export default class ClassAPI {
  constructor($axios) {
    this.$axios = $axios
  }
  getClasses(status, start_date, end_date, sort, page = 1, limit = 10) {
    return this.$axios.request({
      'url': url + '/v1/me/class',
      'params': {
        status,
        start_date,
        end_date,
        sort,
        page,
        limit
      },
      'method': 'GET',
      'headers': {
        'Content-Type': 'application/json'
      }
    })
  }
  createClass(data) {
    return this.$axios.request({
      'url': url + '/v1/class',
      'method': 'POST',
      data,
      'header': {
        'Content-Type': 'application/json'
      }
    })
  }
  editClass(id, data) {
    return this.$axios.request({
      'url': url + '/v1/class/' + id,
      'method': 'PUT',
      data,
      'header': {
        'Content-Type': 'application/json'
      }
    })
  }
  deleteClasses(data) {
    return this.$axios.request({
      'url': url + '/v1/class',
      'method': 'DELETE',
      data,
      'header': {
        'Content-Type': 'application/json'
      }
    })
  }
  getClass(id) {
    return this.$axios.request({
      'url': url + '/v1/class/' + id,
      'method': 'GET',
      'header': {
        'Content-Type': 'application/json'
      }
    })
  }
}
