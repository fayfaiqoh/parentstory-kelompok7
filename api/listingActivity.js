/*
* @Author: Arthur Pello
* @Date:   2018-08-15 12:30:48
* @Last Modified by:   Arthur Pello
* @Last Modified time: 2018-08-20 15:52:26
*/

const url = process.env.API_BASE_URL + 'ms/class'
const today = new Date().toISOString().slice(0,10)

export default class ClassListingAPI{
	constructor($axios) {
		this.$axios = $axios
	}

	getActivities(date = today, size = 10, page = 1, tags = null, age_range = null, city= null, minimum_time = "00:00", maximum_time = "17:00") {
		let params = {
			page, size, minimum_time, maximum_time
		}

		// if (tags != '' && age_range != '') {
		// 	params = { page, size, minimum_time, maximum_time, tags, age_range }
		// } else if (tags != '' && age_range == '') {
		// 	params = { page, size, minimum_time, maximum_time, tags }
		// } else if (tags == '' && age_range != '') {
		// 	params = { page, size, minimum_time, maximum_time, age_range }
		// }

		if (tags != '')
			params.tags = tags
		if (age_range != '')
			params.age_range = age_range
		if (city != '')
			params.city = city

		return this.$axios.request({
			'url': url + '/v1/list/' + date,
			'params': params,
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}

	getSearchCompletion(q){
		return this.$axios.request({
			'url': url + '/v1/search/completion',
			'params': {
				q
			},
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}

	searchActivities(q, size=10, page=1, tags = null, age_range = null,city=null){
		let params = {
			page, size, q
		}

		if (tags != '')
			params.tags = tags
		if (age_range != '')
			params.age_range = age_range
		if (city != '')
			params.city = city

		return this.$axios.request({
			'url': url + '/v1/search',
			'params': params,
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}

	getPartners(page = 1, size = 100) {
		return this.$axios.request({
			'url': url + '/v1/partners',
			'params': {
				page,
				size
			},
			'method': 'GET',
			'headers': {
				'Content-Type': 'application/json'
			}
		}).then(response => response.data.data)
	}
}