const url = process.env.API_BASE_URL + 'ms/activity'

export default class AgeRangeAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getAgeRanges () {
		return this.$axios.request({
			'url': url + '/v1/age_ranges',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getAgeGroups() {
		return this.$axios.request({
			'url': url + '/v1/age_groups',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
