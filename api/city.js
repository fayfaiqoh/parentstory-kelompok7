const url = process.env.API_BASE_URL + 'ms/activity'

export default class ClassAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getCities () {
		return this.$axios.request({
			'url': url + '/v1/cities',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
