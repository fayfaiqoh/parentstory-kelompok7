const url = process.env.API_BASE_URL + 'ms/activity'

export default class FavoriteAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	doFavorite (data) {
		return this.$axios.request({
			'url': url + '/v1/favorite',
			'method': 'POST',
			data,
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	deleteFavorite (id) {
		return this.$axios.request({
			'url': url + '/v1/favorite/'+id,
			'method': 'DELETE',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getMyFavorites(page=1,limit=10) {
		return this.$axios.request({
			'url': url + '/v1/me/favorite',
			'method': 'GET',
			'params':{
				page,
				limit
			},
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
