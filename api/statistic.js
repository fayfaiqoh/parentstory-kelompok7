const url = process.env.API_BASE_URL + 'ms/activity'

export default class StatisticAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getGeneralStatistic(start_date, end_date) {
		return this.$axios.request({
			'url': url + '/v1/statistic/general',
			'method': 'GET',
			'params': {
				start_date,
				end_date
			},
			'headers': {
				'Content-Type': 'application/json'
			}
		})
	}
	getTopActivitiesStatistic(start_date, end_date) {
		return this.$axios.request({
			'url': url + '/v1/statistic/top_activities',
			'method': 'GET',
			'params': {
				start_date,
				end_date
			},
			'headers': {
				'Content-Type': 'application/json'
			}
		})
	}
}
