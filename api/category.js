const url = process.env.API_BASE_URL + 'ms/activity'

export default class CategoryAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getCategories () {
		return this.$axios.request({
			'url': url + '/v1/categories',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getActiveCategories () {
		return this.$axios.request({
			'url': url + '/v1/categories/active',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
