const url = process.env.API_BASE_URL + 'ms/account'

export default class PartnerAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	updatePartner (data) {
		return this.$axios.request({
			'url': url + '/v1/partner',
			'method': 'PUT',
			data,
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}
	getPartner(id,attr_type="id"){
		return this.$axios.request({
			'url': url + '/v1/partner/'+id,
			'method': 'GET',
			'params': {
				attr_type
			},
			'header': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
