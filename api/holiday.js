const url = process.env.API_BASE_URL + 'ms/activity'

export default class HolidayAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getHolidays () {
		return this.$axios.request({
			'url': url + '/v1/holidays',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
}
