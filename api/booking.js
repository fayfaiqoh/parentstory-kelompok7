const url = process.env.API_BASE_URL + 'ms/booking'
const pay_url = process.env.API_BASE_URL + 'ms/pay'
const base_url = process.env.BASE_URL

export default class BookingAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getClassParticipant (class_id,page=1,size=10) {
		return this.$axios.request({
			'url': url + '/v1/class/' +class_id+ '/bookings',
			'method': 'GET',
			'params': {
				page,
				size
			},
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getBookingEligibility (class_id) {
		return this.$axios.request({
			'url': url + '/v1/me/eligibility',
			'method': 'GET',
			'params': {
				class_id,
				partner_id: 0
			},
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	doBook(data){
		return this.$axios.request({
			'url': url + '/v1/book',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	getMyBookings(status=0,page=1,size=10){
		return this.$axios.request({
			'url': url + '/v1/me/bookings',
			'method': 'GET',
			'params':{
				status,
				page,
				size
			},
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}

	getBookingDetail(id){
		return this.$axios.request({
			'url': url + '/v1/booking/'+id,
			'method': 'GET',
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}

	cancelBooking(data){
		return this.$axios.request({
			'url': url + '/v1/cancel',
			'method': 'PUT',
			data,
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}

	activateBooking(data){
		return this.$axios.request({
			'url': url + '/v1/present',
			'method': 'PUT',
			data,
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}

	getAvailableMemberships () {
		return this.$axios.request({
			'url': url + '/v1/memberships',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getMemberships() {
	  return this.$axios.request({
	    'url': url + '/v1/me/memberships',
	    'method': 'GET',
	    'headers': {
	      'Content-Type': 'application/json'
	    }
	  })
	}

	getTransactionList() {
		return this.$axios.request({
		  'url': url + '/v1/me/purchases',
		  'method': 'GET',
		  'headers': {
			'Content-Type': 'application/json'
		  }
		})
	}

	getTransactionDetail(paymentCode) {
		return this.$axios.request({
		  'url': url + '/v1/purchase/'+paymentCode,
		  'method': 'GET',
		  'headers': {
			'Content-Type': 'application/json'
		  }
		})
	}

	addMemberships(data){
		return this.$axios.request({
			'url': url + '/v1/memberships',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}

	doPayment(obj){
		let data = {
			product_items:[
				{
				  data: {
					memberships_package_id: obj.package_id,
					parents_memberships_package_id: obj.membership_id,
					basic_price: obj.price,
					duration: "month"
				  },
				  product_id: 3,
				  quantity: 1
				}
			  ],
			  targeted_currency : "IDR",
			  success_url: base_url + 'parent/subscriptions'
		}
		return this.$axios.request({
			'url': pay_url + '/v1/payments',
			'method': 'POST',
			data,
			'headers':{
				'Content-Type' : 'application/json',
				'Session-Id': 'Bearer ' + obj.token
			}
		})
	}
	
	getParentPastBookings(page = 1, size = 10) {
		return this.getMyBookings(1, page, size)
	}
	getParentUpcomingBookings(page = 1, size) {
		return this.getMyBookings(0, page, size)
	}
}
