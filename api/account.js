const url = process.env.API_BASE_URL + 'ms/account'

export default class AccountAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	login (data) {
		return this.$axios.request({
			'url': url + '/v1/login',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	registerPartner (data) {
		return this.$axios.request({
			'url': url + '/v1/register_partner',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	registerParent (data) {
		return this.$axios.request({
			'url': url + '/v1/register',
			'method': 'POST',
			data,
			'header': {
				'Content-Type': 'application/json'
			}
		})
	}
	getProfile () {
		return this.$axios.request({
			'url': url + '/v1/me',
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	refreshToken (data) {
		return this.$axios.request({
			'url': url + '/v1/refresh_token',
			'method': 'POST',
			data,
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	requestResetPassword(data) {
		return this.$axios.request({
			url: url + "/v1/forgot_password",
			method: "POST",
			data,
			headers: {
				"Content-Type": "application/json"
			}
		})
	}
	doResetPassword(data) {
		return this.$axios.request({
			url: url + "/v1/forgot_save",
			method: "POST",
			data,
			headers: {
				"Content-Type": "application/json"
			}
		})
	}
	changePassword(data){
		return this.$axios.request({
			url: url + "/v1/change_password",
			method: "POST",
			data,
			headers: {
				"Content-Type": "application/json"
			}
		})
	}
	updateParent(data) {
		return this.$axios.request({
			url: url + "/v1/parent",
			method: "PUT",
			data,
			headers: { "Content-Type": "application/json" }
		})
	}
	requestSMSVerificationCode() {
		return this.$axios.request({
			url: url + "/v1/send_verification",
			method: "POST",
			data: { },
			headers: {
				"Content-Type": "application/json"
			}
		})
	}
	verify(data) {
		return this.$axios.request({
		  url: url + "/v1/verify",
		  method: "POST",
		  data,
		  headers: {
		    "Content-Type": "application/json"
		  }
		})
	}
	skipVerification() {
		return this.$axios.request({
		  url: url + "/v1/skip_verification",
		  method: "POST",
		  data: { },
		  headers: {
		    "Content-Type": "application/json"
		  }
		})
	}
}
