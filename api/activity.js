const url = process.env.API_BASE_URL + 'ms/activity'

export default class ActivityAPI{
	constructor($axios) {
		this.$axios = $axios
	}
	getActivities (keyword="",status=null,sort=null,page=1,limit=10) {
		return this.$axios.request({
			'url': url + '/v1/me/activity',
			'params': {
				keyword,
				sort,
				status,
				page,
				limit
			},
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getActivity (id) {
		return this.$axios.request({
			'url': url + '/v1/activity/'+id,
			'method': 'GET',
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	getClassByActivity (id,status=null,sort=null,page=1,limit=10) {
		return this.$axios.request({
			'url': url + '/v1/activity/'+id+'/class',
			'method': 'GET',
			'params': {
				page,
				limit,
				status,
				sort
			},
			'headers': {
				'Content-Type' : 'application/json'
			}
		})
	}
	createActivity(data){
		return this.$axios.request({
			'url': url + '/v1/activity',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	editActivity(id,data){
		return this.$axios.request({
			'url': url + '/v1/activity/'+id,
			'method': 'PUT',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	saveActivity(id,data){
		return this.$axios.request({
			'url': url + '/v1/activity/'+id+'/save',
			'method': 'POST',
			data,
			'header':{
				'Content-Type' : 'application/x-www-form-urlencoded'
			}
		})
	}
	deleteActivities(data){
		return this.$axios.request({
			'url': url + '/v1/activity',
			'method': 'DELETE',
			data,
			'header':{
				'Content-Type' : 'application/json'
			}
		})
	}
	getUpcomingActivities(id) {
		return this.$axios.request({
		  'url': url + '/v1/partner/' + id + '/upcoming_classes',
		  'method': 'GET',
		  'headers': {
		    'Content-Type': 'application/json'
		  }
		}).then(response => response.data.data)
	}

}
