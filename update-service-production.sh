TASK_REVISION=`aws ecs describe-task-definition --task-definition parentstoryweb-production-task | egrep "revision" | tr "/" " " | awk '{ print $2 }' | sed 's/,$//'`
DESIRED_COUNT=`aws ecs describe-services --services arn:aws:ecs:ap-southeast-1:922278837503:service/parentstoryweb-production-service --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/frontend-production-cluster-private | egrep "desiredCount" | tr "/" " " | awk '{print $2}' | sed 's/,$//' | head -n1`
if [ ${DESIRED_COUNT} = "0" ]; then
    DESIRED_COUNT="1"
fi
aws ecs update-service --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/frontend-production-cluster-private  --service arn:aws:ecs:ap-southeast-1:922278837503:service/parentstoryweb-production-service --task-definition parentstoryweb-production-task:${TASK_REVISION} --desired-count ${DESIRED_COUNT}
