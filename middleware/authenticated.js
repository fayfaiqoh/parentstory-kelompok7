import accountAPI from '@/api/account'

export default function ({ store, redirect ,$axios}) {
		// If the user is not authenticated
		if (!store.state.account.token){
			let redirectUrl = store.state.lang.locale == 'en' ? '/login' : '/' + store.state.lang.locale + '/login'
			return store.dispatch("logout"),redirect(redirectUrl)
		} else if (!store.state.account.account) {
			var api = new accountAPI($axios)
			return api.getProfile().then(response => {
				store.dispatch('setAccount', response.data.data)
			})
		}
	}