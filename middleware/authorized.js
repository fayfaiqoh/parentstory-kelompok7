export default function ({ store, redirect, route }) {
	if (route.path.startsWith('/dashboard')) {
		if (store.state.account.account.type !== 'partner') {
			return redirect('/activity')
		}
	}
}