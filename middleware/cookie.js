import Cookie from 'js-cookie'
import cookieparser from 'cookieparser'

export default function ({ store, req ,$axios }) {
	let token = {}
	if (process.server && req.headers.cookie) {
		var parsed = cookieparser.parse(req.headers.cookie)
		token.accessToken = parsed.PSID
		token.refreshToken = parsed.PSIDRT
		if (parsed.PSID)
			$axios.setToken(parsed.PSID,'Bearer')
	}
	else if (process.client){
		token.accessToken = Cookie.get('PSID')
		token.refreshToken = Cookie.get('PSIDRT')
		if (Cookie.get('PSID'))
			$axios.setToken(Cookie.get('PSID'),'Bearer')
	}
	store.commit('SET_TOKEN', token)
}
