export default function ({ store, redirect }) {
	if (store.state.account.account.type == 'parent' &&
		!store.state.account.account.is_verified) {
		return redirect('/verification')
	}
}