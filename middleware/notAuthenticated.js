export default function ({ store, redirect }) {
		// If the user is authenticated redirect to home page
		if (store.state.account.token) {
			let redirectUrl = store.state.lang.locale == 'en' ? '/' : '/' + store.state.lang.locale + '/'
			return redirect(redirectUrl)
		}
	}