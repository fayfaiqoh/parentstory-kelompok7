export function Benefit(imageUrl, alt, title, desc) {
	this.imageUrl = imageUrl;
	this.alt = alt;
	this.title = title;
	this.desc = desc;
}

export function Testimony(imageUrl, alt, text, giver) {
	this.imageUrl = imageUrl;
	this.alt = alt;
	this.text = text;
	this.giver = giver;
}

export function Field() {

}