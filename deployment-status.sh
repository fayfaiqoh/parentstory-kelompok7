CLUSTER_NAME='frontend-staging-cluster-private'
SERVICE_NAME='parentstoryweb-staging-service'
CONTAINER_STATUS='steady'



getEvents(){
	eventStatus=`aws ecs describe-services --services arn:aws:ecs:ap-southeast-1:922278837503:service/$SERVICE_NAME --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/$CLUSTER_NAME | grep 'events' -A5 | grep 'message' | awk '{ print $7 }'`
	ecsMsg=`aws ecs describe-services --services arn:aws:ecs:ap-southeast-1:922278837503:service/$SERVICE_NAME --cluster arn:aws:ecs:ap-southeast-1:922278837503:cluster/$CLUSTER_NAME | grep 'events' -A5 | grep 'message'`

}

sleep 30s

getEvents

until [[ "$eventStatus" = "$CONTAINER_STATUS" ]]; do
	#statements
	getEvents
	echo 'Container Status =' $ecsMsg
	sleep 5
done
echo 'Container Status =' $eventStatus
exit 0

