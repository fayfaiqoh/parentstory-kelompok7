# Parentstory

> Parentstory Web

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).

If `npm run dev` failed, try checking your node version. Use version >= 8.

## Style Guide

Hopefully there will be lint check in the future. In the meantime, please read and
obey this rule.

1. Use tabs.
2. Don't use semicolon.
3. [Use minimal 2 words for component name](https://vuejs.org/v2/style-guide/#Multi-word-component-names-essential).
